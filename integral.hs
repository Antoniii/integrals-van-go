rightRectanglesIintegration :: Double -> Double -> Integer -> (Double -> Double) -> Double
rightRectanglesIintegration xmin xmax intervals f =
    let step = (xmax - xmin) / fromInteger intervals
        x1 = xmin + step
        x2 = x1   + step
        s = sum $ map f [x1,x2 .. xmax]
    in  s * step
 
 
main :: IO ()
main = print $ rightRectanglesIintegration 1.2 2 100000 $ \ x -> (x-0.5)/sqrt(x*x-1)

--rightRectanglesIntegration :: Double -> Double -> Integer -> (Double -> Double) -> Double
--rightRectanglesIntegration xmin xmax intervals f =
--    let step = stepIntegration xmin xmax intervals
--        s  = sumIntegration (xmin + step) xmax step f
--    in s * step
 
--trapezoidalIntegration :: Double -> Double -> Integer -> (Double -> Double) -> Double
--trapezoidalIntegration xmin xmax intervals f =
--    let step = stepIntegration xmin xmax intervals
--        s  = sumIntegration (xmin + step) (xmax - step) step f
--    in (s + (f xmin + f xmax)/2) * step
 
---- вспомогательные
--sumIntegration :: Double -> Double -> Double -> (Double -> Double) -> Double
--sumIntegration xFirst xLast step f = sum $ map f [xFirst, xFirst + step .. xLast]
 
--stepIntegration :: Double -> Double -> Integer -> Double 
--stepIntegration xmin xmax intervals = (xmax - xmin) / fromInteger intervals
 
---- Основная функция
--main :: IO ()
--main = do
--    let f x = (x-0.5)/sqrt(x*x-1)
--        example title method = putStr title >> putStr " : " >>
--            print (method 1.2 2 100000 f)
--    example "Right rectangle method" rightRectanglesIntegration
--    example "Trapezoidal method    " trapezoidalIntegration


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Antoniii/integrals-van-go.git
git branch -M main
git push -uf origin main
```

![](https://gitlab.com/Antoniii/integrals-van-go/-/raw/main/integrals.png)


![](https://gitlab.com/Antoniii/integrals-van-go/-/raw/main/testgo.PNG)


![](https://gitlab.com/Antoniii/integrals-van-go/-/raw/main/test.PNG)


# Sources

* [Решение на Python: метод левых, правых и средних прямоугольников; трапеции; Симпсона; полиномов Лагранжа и Лежандра 8-го порядка](https://github.com/Antoniii/lab1/blob/master/lab1.py)
* [Решение на Python: метод полиномов Лежандра 64-го порядка](https://github.com/Antoniii/legender-64/blob/master/legendre.py)
* [Численное интегрирование с Питоном](https://gitlab.com/Antoniii/integrals)
* [Численные методы (Самарский университет)](https://github.com/ochaplashkin/numerical_analysis)
* [Решение на Haskell: метод правых прямоугольников](https://www.cyberforum.ru/haskell/thread2554650.html)
* [Решение на Haskell: метод трапеций](https://www.cyberforum.ru/haskell/thread2556043.html)
* [Haskell online editor, IDE, compiler, interpreter, and REPL](https://replit.com/languages/haskell)

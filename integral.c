#include <stdio.h>
#include <math.h> // gcc integral.c -o integral -lm

double f(double x){
	return tan(x*x);
}

void main(){
	double a = 0, b = 1.2, s = 0.0, h;
	int N, i;
	// printf("Input a>");
	// scanf("%lf", &a);
	// printf("Input b>");
	// scanf("%lf", &b);
	printf("Input N>");
	scanf("%d", &N);
	h = (b - a)/ N;
	for (i = 0; i < N; i++)	{
		s += f(a+i*h)*h;
	}
	printf("Integral = %lf\n", s);
}

// https://www.wolframalpha.com/input?i=integrate+tan%28x%5E2%29+dx+from+0+to+1.2
//  = 1.02995

// Input N>100
// Integral = 0.986020

// Input N>1000
// Integral = 1.025402

// Input N>10000
// Integral = 1.029491

// Input N>100000
// Integral = 1.029901
 
// Input N>1000000
// Integral = 1.029942
 
// Input N>10000000
// Integral = 1.029946
 
// Input N>100000000
// Integral = 1.029946
